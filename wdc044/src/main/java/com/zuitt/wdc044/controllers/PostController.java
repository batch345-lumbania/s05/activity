package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.ArrayList;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<?> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {

        postService.createPost(stringToken, post);

        return new ResponseEntity<>("Post Created Successfully!", HttpStatus.CREATED);
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<?> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @RequestMapping(value="/posts/{postId}", method=RequestMethod.PUT)
    public ResponseEntity<?> updatePost(@RequestHeader(value="Authorization") String stringToken, @RequestBody Post post, @PathVariable Long postId) {
        return postService.updatePost(postId, stringToken, post);
    }

//    This route is for deleteion of specific post
    @RequestMapping(value="/posts/{postId}", method=RequestMethod.DELETE)
    public ResponseEntity<?> deletePost(@RequestHeader(value="Authorization") String stringToken, @PathVariable Long postId) {
        return postService.deletePost(postId, stringToken);
    }

    @RequestMapping(value="/myPosts", method=RequestMethod.GET)
    public ResponseEntity<?> myPosts(@RequestHeader(value="Authorization") String stringToken) {
        ArrayList<Post> myPostsArray = postService.getMyPosts(stringToken);

        if (myPostsArray.isEmpty()) {
            return new ResponseEntity<>("You do not have an existing post", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(myPostsArray, HttpStatus.OK);
        }
    }
}
