package com.zuitt.wdc044.services;

//We need to have access directly to the users Table through the User model
import com.zuitt.wdc044.models.User;

//We need to have access with the UserRepository because we will be needing the methods inside that repository
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

//THe component annotation informs or declares that our class is a component class
import org.springframework.stereotype.Component;

//This object contains the userdetails
import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.core.userdetails.UserDetailsService;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;

@Component
public class JwtUserDetailsService implements UserDetailsService{
    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with the username: " + username);
        }

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());
    }

}
