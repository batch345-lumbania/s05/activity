package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.zuitt.wdc044.config.JwtToken;

import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.UserRepository;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    JwtToken jwtToken;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void createPost(String stringToken, Post post) {
        String username = jwtToken.getUsernameFromToken(stringToken);
        User author = userRepository.findByUsername(username);

        Post newPost = new Post(post.getTitle(), post.getContent(), author);

        postRepository.save(newPost);
    }

    public Iterable<Post> getPosts() {
        //This method is from the CrudRepository wherein it will contain or get all the records inside its table.
        return postRepository.findAll();
    }

    public ResponseEntity<?> updatePost(Long id, String stringToken, Post post) {
        //        We try to capture the username of the author who posted this specific post
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthor = postForUpdating.getUser().getUsername();

        // We are going to get the username of the user who owns the provided Token
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if (postAuthor.equals(authenticatedUser)) {
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to edit this post", HttpStatus.UNAUTHORIZED);
        }
    }

//    Implementation of deletePost action from the PostService

    public ResponseEntity<?> deletePost(Long id, String stringToken) {
        Optional<Post> postForDeleting = postRepository.findById(id);
        if (postForDeleting.isEmpty()) {
            return new ResponseEntity<>("Post does not exist", HttpStatus.BAD_REQUEST);
        }

        String postAuthor = postForDeleting.get().getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);


        if (authenticatedUsername.equals(postAuthor)) {
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post successfully deleted", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You are not authorized to delete this post", HttpStatus.UNAUTHORIZED);
        }
    }

    public ArrayList<Post> getMyPosts(String stringToken) {
        ArrayList<Post> myPosts = new ArrayList<Post>();
        String username = jwtToken.getUsernameFromToken(stringToken);
        User userDetails = userRepository.findByUsername(username);
        Iterable<Post> allPosts = postRepository.findAll();

        for(Post post : allPosts) {
            if (post.getUser().getId().equals(userDetails.getId())) {
                myPosts.add(post);
            }
        }
        return myPosts;
    }
}
